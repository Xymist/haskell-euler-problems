-- Euler Q7 TODO: Work out how to recurse ONLY on already-found primes. This takes about an hour for getPrime 10000 (although it remembers the list, which speeds things up a bit)

modList n = [a | a <- [1..n], n `mod` a == 0]

isPrime n 
  | length (modList n) == 2 = True
  | otherwise = False

findPrimes = [ a | a <- [1..], isPrime a == True]

getPrime n = (findPrimes) !! n


-- End Euler Q7