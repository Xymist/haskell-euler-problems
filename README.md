That which is in a folder has been compiled; probably for Windows, maybe for Linux depending on which computer I was using that day. There's usually an original version and an optimised version; the latter will exist because the former takes an inordinate length of time to run, or possibly has a memory leak.

Since most of these have been in the position at one point of having been ridiculously slow and/or incorrect, there's not *much* value in going back to look at the old commits. Unless you like rubbish code, of course.
