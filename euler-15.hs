factorial :: Integer -> Integer
factorial 1 = 1  
factorial n = n * factorial (n-1)

n `choose` k
    | k < 0     = 0
    | k > n     = 0
    | otherwise = factorial n `div` (factorial k * factorial (n-k))

-- For this problem, n is 40 and k is 20. Yes, this will get bogged down on large numbers because it's not memoised. I might rewrite it at some point.