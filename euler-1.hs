divThreeFive n
    |n `mod` 3 == 0 = True
    |n `mod` 5 == 0 = True
    |otherwise = False

threeFives = sum [num | num <- [1..999], divThreeFive num]