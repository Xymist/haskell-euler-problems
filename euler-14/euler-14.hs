collatzSequence 1 = [1]  
collatzSequence n
 | even n =  n:collatzSequence (n `div` 2)  
 | odd n  =  n:collatzSequence (n*3 + 1) 

collatzes n = [ collatzSequence x | x <- [n, n-1..(n `div` 2)]] -- List of lists which are Collatz strings

headLength [] = (0,0)
headLength (x:xs) = ((head (x:xs)), (length (x:xs)))

collatzHeadToLength n = [ x | x <- filter ((>444).snd) (map headLength (collatzes n))]