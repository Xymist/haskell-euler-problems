collatzSequence :: Int -> [Int]
collatzSequence 1 = []  
collatzSequence n
 | even n =  n:collatzSequence (n `div` 2)
 | odd n  =  n:collatzSequence (n*3 + 1) 

collatzTuples :: [Int] -> [(Int, Int)]
collatzTuples [] = []
collatzTuples (x:xs) = (x, length (x:collatzSequence(x))) : collatzTuples xs

main :: IO ()
main = do
    limStr <- getLine
    let lim 
         | (read limStr) `mod` 2 /= 0 = read limStr -- This allows us to test only odd numbers
         | otherwise = (read limStr) + 1
    print (foldr biggerPair (1,1) (collatzTuples [lim,lim-2..(lim `div` 2)])) -- Only need to test down to half the initial value, due to the n/2 operation
      where
        biggerPair (x1,len1) (x2,len2)
         | len1 > len2 = (x1,len1)
         | len1 < len2 = (x2,len2)
         | otherwise = (x2,len2)