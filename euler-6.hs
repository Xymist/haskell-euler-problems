-- Euler Q6

square n = n*n

sumSquares a b = sum [square n | n <- [a..b]]

squareSum a b = (sum [a..b])^2

sumSquareDiff a b = abs (sumSquares a b - squareSum a b)