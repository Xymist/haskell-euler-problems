{-factors n = [ x | x <- [1..n], n `mod` x == 0]
isPrime n = factors n == [1,n]

primeBuilder = [ x | x <- [3,5..], isPrime x]
primes n = take n (1:2:primeBuilder)

primeFactors n = [ x | x <- (factors n), isPrime x == True]-}

--This is ridiculously slow. Like, O(n^2) slow. Archived for historical interest only.
import Minus

betterPrimes m = 2 : sieve [3,5..m]
  where
    sieve (p:xs) 
       | p*p > m   = p : xs
       | otherwise = p : sieve (xs `minus` map (p*) [p,p+2..])

primeFactors k = [ x | x <- (betterPrimes k), k `mod` x == 0]