-- Problem:
-- A gambler decides to participate in a special lottery. In this lottery the gambler plays a series of one or more games.
-- Each game costs m pounds to play and starts with an initial pot of 1 pound. The gambler flips an unbiased coin. 
-- Every time a head appears, the pot is doubled and the gambler continues. When a tail appears, the game ends and the gambler collects the current value of the pot. 
-- The gambler is certain to win at least 1 pound, the starting value of the pot, at the cost of m pounds, the initial fee.
--
-- The gambler cannot continue to play if his fortune falls below m pounds.
-- Let pm(s) denote the probability that the gambler will never run out of money in this lottery given his initial fortune s and the cost per game m.
-- For example p2(2) ≈ 0.2522, p2(5) ≈ 0.6873 and p6(10 000) ≈ 0.9952 (note: pm(s) = 0 for s < m).
--
-- Find p15(10^9)

wealth = 10^9
playCost = 15
initialPot = 1

tailsProbability n = 1-(0.5**n) -- Probability that the gambler will have flipped a tails by turn n (explicitly NOT the probability that turn n will be tails, which is always 0.5) 

winAmount n = (2**(n-1)) - (n*playCost) -- Increase in wealth upon Tails after n coinflips

expectedWin n = (winAmount n)*(tailsProbability n)

{-I gave up on this. I'm not even ashamed; I went and looked up the mathematical problem it's based on and didn't even understand the question. I don't yet have that level of mathematical knowledge. 27/11/15-}