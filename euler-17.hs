spellNums = ["one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety","hundred","thousand"]

hundredFlat n = (n - (rem n 100)) `div` 100
thousandFlat n = (n - (rem n 1000)) `div` 1000

spellNum n
  | n > 0 && n <= 20 = spellNums !! (n-1)
  | n > 20 && n < 30 = (spellNum 20) ++ "-" ++ (spellNum (n-20))
  | n == 30 = spellNums !! 20
  | n > 30 && n < 40 = (spellNum 30) ++ "-" ++ (spellNum (n-30))
  | n == 40 = spellNums !! 21
  | n > 40 && n < 50 = (spellNum 40) ++ "-" ++ (spellNum (n-40))
  | n == 50 = spellNums !! 22
  | n > 50 && n < 60 = (spellNum 50) ++ "-" ++ (spellNum (n-50))
  | n == 60 = spellNums !! 23
  | n > 60 && n < 70 = (spellNum 60) ++ "-" ++ (spellNum (n-60))
  | n == 70 = spellNums !! 24
  | n > 70 && n < 80 = (spellNum 70) ++ "-" ++ (spellNum (n-70))
  | n == 80 = spellNums !! 25
  | n > 80 && n < 90 = (spellNum 80) ++ "-" ++ (spellNum (n-80))
  | n == 90 = spellNums !! 26
  | n > 90 && n < 100 = (spellNum 90) ++ "-" ++ (spellNum (n-90))
  | n >= 100 && n < 1000 && n `mod` 100 /= 0 = (spellNum (hundredFlat n)) ++ " " ++ (spellNums !! 27) ++ " and " ++ (spellNum (n `mod` 100))
  | n >= 100 && n < 1000 && n `mod` 100 == 0 = (spellNum (hundredFlat n)) ++ " " ++ (spellNums !! 27)
  | n >= 1000 && n < 1000000 && n `mod` 1000 /= 0 = (spellNum (thousandFlat n)) ++ " " ++ (spellNums !! 28) ++ " " ++ (spellNum (n `mod` 1000))
  | n >= 1000 && n < 1000000 && n `mod` 1000 == 0 = (spellNum (thousandFlat n)) ++ " " ++ (spellNums !! 28)
  | otherwise = "Numbers that big don't have names!"
