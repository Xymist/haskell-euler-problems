import Data.List

isPalindrome [] = True
isPalindrome (x:xs)
  |reverse (x:xs) == (x:xs) = True
  |otherwise = False

findPali n = [ x | x <- [1..n], isPalindrome (show x)]

productPals a b = maximum [ x*y | x <- [a..b], y <- [a..b], isPalindrome (show (x*y))]