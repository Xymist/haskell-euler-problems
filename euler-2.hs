fibonacci = 0:1:zipWith (+) fibonacci (tail fibonacci)
evenFibs =  [ x | x <- (take 50 fibonacci), x <= 4000000, even x]