indivisible :: Int -> Int -> Bool
indivisible n x = n `mod` x /= 0

divisibleTwenty :: Int -> Bool
divisibleTwenty n = all (==False) [indivisible n x | x <- [2..20]] -- Returns True iff N is divisible by every number under 20

smallestD20 = head [num | num <- [20,40..], divisibleTwenty num == True] -- Starting at 1*20 and incrementing in 20s, tests for full divisibility until it finds one, then returns.